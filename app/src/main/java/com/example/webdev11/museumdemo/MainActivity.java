package com.example.webdev11.museumdemo;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

public class MainActivity extends Activity {

    AugmentedSurface augmentedSurface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        FrameLayout layout = (FrameLayout) findViewById(R.id.frame_layout);

        CameraSurface cameraSurface = new CameraSurface(this);
        if (cameraSurface.isCameraAvailable()) {
            layout.addView(cameraSurface);
            augmentedSurface = new AugmentedSurface(this);
            layout.addView(augmentedSurface);

        } else {
            setContentView(R.layout.error_view);
        }
    }

    @Override public boolean onKeyUp(int keyCode, KeyEvent event)     {
        Log.e("OnKeyUp", Integer.toString(keyCode));
        if (this.augmentedSurface != null && keyCode == 66) {
            augmentedSurface.setKeyPressed();
        }
        return false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (this.augmentedSurface != null) {
            this.augmentedSurface.stop();
        }
    }

    public void displayOptions() {
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.more_information_layout, null);
        v.findViewById(R.id.villa_button).requestFocus();
        FrameLayout layout = (FrameLayout) findViewById(R.id.frame_layout);
        layout.addView(v);
        v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_to_left));

    }

    public void removeOptions() {
        View options = findViewById(R.id.more_information);
        options.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_to_right));
        ((ViewGroup) options.getParent()).removeView(options);
    }

    public void showVillas(View view) {
        removeOptions();
        this.augmentedSurface.showVilla();
    }

    public void showArmy(View view) {
        removeOptions();
        this.augmentedSurface.showArmy();
    }

    public void backToStart(View view) {
        removeOptions();
        this.augmentedSurface.backToStart();
    }

    public void showTraining(View view) {
        removeOptions();
        this.augmentedSurface.showTraining();
    }

}
