package com.example.webdev11.museumdemo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Movie;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by webdev11 on 2016-07-18.
 */
public class AugmentedSurface extends View {

    private enum MovieState {
        Intro,
        WaitForStart,
        StartIntro,
        ShowTimeline,
        ShowVilla,
        ShowArmy,
        ShowMap,
        EmpireEnds,
        MoreInformation,
        Training
    }

    private int learnRomanAudio = R.raw.learn_about_romans;
    private int swordAudio = R.raw.rage_of_blades;
    private int timelineAudio = R.raw.roman_timeline_audio;
    private int villaAudio = R.raw.villa_audio;
    private int marchingAudio = R.raw.soldiers_marching;
    private int armyAudio = R.raw.roman_army_audio;
    private int mapAudio = R.raw.roman_empire_audio;
    private int fallEmpireAudio = R.raw.fall_of_rome_audio;
    private int crashAudio = R.raw.large_crash;
    private int moreInformationAudio = R.raw.more_information_audio;
    private int trainingAudio = R.raw.training_audio;

    private int attackGif = R.raw.septimus_attacks2;
    private int blockGif = R.raw.septimus_blocks;
    private int villaGif = R.raw.villa_rotation;
    private int armyGif = R.raw.army_run;
    private int trainingGif = R.raw.roman_battle;
    private int deathGif = R.raw.fall_of_rome;

    private int timelineImage = R.drawable.roman_timeline;
    private Bitmap timeline;
    private int timelineCount = 400;

    private ArrayList<InputStream> videoStreams = new ArrayList<InputStream>();

    private int startIntroCount = -130;
    private float showArmyCount = 0.05f;
    private float showArmyIncrement = 0.0001f;

    private Movie attackMovie;
    private Movie blockMovie;
    private Movie runMovie;
    private Movie villaMovie;
    private Movie trainingMovie;
    private Movie dieMovie;
    private long mMovieStart;
    private MovieState state;
    private MainActivity context;
    private boolean loaded = false;
    private boolean crashPlayed = false;
    private MediaPlayer mediaPlayer;

    public AugmentedSurface(MainActivity context) {
        super(context);
        this.context = context;
        timeline = BitmapFactory.decodeResource(getResources(), timelineImage);
        if (this.attackMovie == null) {
            new LoadStreamTask().execute();
            new LoadExtraStreamTask().execute();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!loaded) {
            return;
        }
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        switch (this.state) {
            case Intro:
                int relTime = (int)(SystemClock.uptimeMillis() - mMovieStart);
                if (relTime < attackMovie.duration()) {

                    attackMovie.setTime(relTime);
                    attackMovie.draw(canvas, -130, -110);
                } else {
                    this.state = MovieState.WaitForStart;
                    startAudioPlayer(this.learnRomanAudio, true, 1);
                }
                break;
            case WaitForStart:
                attackMovie.draw(canvas, -130, -110);

                break;
            case StartIntro:
                relTime = (int)(SystemClock.uptimeMillis() - mMovieStart) % blockMovie.duration();
                if (this.startIntroCount > -350) {

                    blockMovie.setTime(relTime);
                    blockMovie.draw(canvas, this.startIntroCount, -150);
                    this.startIntroCount -= 1;
                    if (this.startIntroCount == -330) {
                        startAudioPlayer(this.timelineAudio, true, 1);
                    }
                } else {
                    this.state = MovieState.ShowTimeline;
                    this.invalidate();

                }
                break;
            case ShowTimeline:
                if (this.timelineCount > -900) {
                    canvas.drawBitmap(this.timeline, this.timelineCount, 0, null);
                    timelineCount -= 2;
                } else {
                    this.state = MovieState.ShowMap;
                    this.mMovieStart = SystemClock.uptimeMillis();
                    startAudioPlayer(this.mapAudio, true, 1);
                }

                break;
            case ShowMap:
                Bitmap map = BitmapFactory.decodeResource(getResources(), R.drawable.map_roman_empire);
                Bitmap scaledMap = Bitmap.createScaledBitmap(map, 280, 170, false);
                canvas.drawBitmap(scaledMap, 80, 20, null);
                if (!this.mediaPlayer.isPlaying()){
                    this.state = MovieState.EmpireEnds;
                    startAudioPlayer(this.fallEmpireAudio, true, 1);
                    mMovieStart = SystemClock.uptimeMillis();
                }
                break;
            case EmpireEnds:
                canvas.scale(0.8f, 0.8f);
                relTime = (int)(SystemClock.uptimeMillis() - mMovieStart);
                if (relTime < dieMovie.duration()) {
                    dieMovie.setTime(relTime);
                    dieMovie.draw(canvas, -100, -40);
                    if (relTime > 7000 && !crashPlayed) {
                        startAudioPlayer(this.crashAudio, false, 0.3f);
                        crashPlayed = true;
                    }
                } else {
                    Log.e("Movie duration", Integer.toString(dieMovie.duration()));
                    this.state = MovieState.MoreInformation;
                    startAudioPlayer(this.moreInformationAudio, true, 1);
                    this.context.displayOptions();
                    showArmyCount = 0.05f;
                    startIntroCount = -130;
                    timelineCount = 400;
                }
                break;
            case MoreInformation:
                Bitmap waiting = BitmapFactory.decodeResource(getResources(), R.drawable.septimus_waits);
                canvas.scale(1.25f, 1.25f);
                canvas.drawBitmap(waiting, -90, -80, null);
                break;
            case ShowVilla:
                canvas.scale(0.5f, 0.5f);
                relTime = (int)(SystemClock.uptimeMillis() - mMovieStart);
                if (relTime < villaMovie.duration()) {

                    villaMovie.setTime(relTime);
                    villaMovie.draw(canvas, 0, 0);
                } else {
                    this.state = MovieState.MoreInformation;
                    startAudioPlayer(this.moreInformationAudio, true, 1);
                    this.context.displayOptions();
                }
                break;
            case ShowArmy:
                canvas.translate((float) (170 - 570 * this.showArmyCount), (float) (100 - 350*this.showArmyCount));
                canvas.scale(this.showArmyCount, this.showArmyCount);
                relTime = (int)(SystemClock.uptimeMillis() - mMovieStart) % runMovie.duration();
                if (this.showArmyCount < 2.5) {
                    runMovie.setTime(relTime);
                    runMovie.draw(canvas, 0, 0);
                    this.showArmyCount += this.showArmyIncrement;
                    this.showArmyIncrement += 0.00001;
                }
                else {
                    this.state = MovieState.MoreInformation;
                    startAudioPlayer(this.moreInformationAudio, true, 1);
                    this.mediaPlayer.setLooping(false);
                    showArmyCount = 0.05f;
                    showArmyIncrement = 0.0001f;
                    this.context.displayOptions();
                }
                break;
            case Training:
                relTime = (int)(SystemClock.uptimeMillis() - mMovieStart) % trainingMovie.duration();
                if (this.startIntroCount < 400) {

                    trainingMovie.setTime(relTime);
                    trainingMovie.draw(canvas, -100, -100);
                    this.startIntroCount += 1;
                } else {
                    this.state = MovieState.MoreInformation;
                    startAudioPlayer(this.moreInformationAudio, true, 1);
                    this.context.displayOptions();
                    this.mediaPlayer.setLooping(false);
                    startIntroCount= -130;

                }
                break;
        }

        this.invalidate();
    }
    private void startAudioPlayer(int audio, boolean cancel, float volume) {
        if (cancel && mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        try {
            mediaPlayer = MediaPlayer.create(getContext(), audio);
            mediaPlayer.setVolume(volume, volume);
            Log.e("Creating", "Audio file");
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setKeyPressed() {
        if (this.state == MovieState.WaitForStart) {
            mMovieStart = SystemClock.uptimeMillis();
            this.state = MovieState.StartIntro;
            showArmyCount = 0.05f;
            startIntroCount = -130;
            timelineCount = 400;
            startAudioPlayer(this.swordAudio, true, 1);
            invalidate();
        }
    }

    public void showArmy(){
        this.mMovieStart = SystemClock.uptimeMillis();
        startAudioPlayer(this.marchingAudio, true, 0.5f);
        this.mediaPlayer.setLooping(true);
        startAudioPlayer(this.armyAudio, false, 1);
        this.state = MovieState.ShowArmy;
    }

    public void showVilla() {
        this.mMovieStart = SystemClock.uptimeMillis();
        startAudioPlayer(this.villaAudio, true, 1);
        this.state = MovieState.ShowVilla;
    }

    public void showTraining() {
        this.mMovieStart = SystemClock.uptimeMillis();
        startAudioPlayer(this.trainingAudio, false, 1);
        startAudioPlayer(this.swordAudio, false, 0.2f);
        this.mediaPlayer.setLooping(true);
        this.state = MovieState.Training;
    }

    public void backToStart() {
        mMovieStart = SystemClock.uptimeMillis();
        startAudioPlayer(swordAudio, true,1);
        state = MovieState.Intro;
        invalidate();
    }

    public void stop() {
        if (this.mediaPlayer != null && mediaPlayer.isPlaying()) {
            this.mediaPlayer.stop();
            this.mediaPlayer.release();
        }
    }

    private class LoadStreamTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... urls) {

            InputStream is = context.getResources().openRawResource(attackGif);

            attackMovie = Movie.decodeStream(is);
            is = context.getResources().openRawResource(blockGif);
            blockMovie = Movie.decodeStream(is);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            state = MovieState.Intro;
            mMovieStart = SystemClock.uptimeMillis();
            startAudioPlayer(swordAudio, true,1);
            loaded = true;
            invalidate();
        }

    }

    private class LoadExtraStreamTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... urls) {

            InputStream is = context.getResources().openRawResource(villaGif);
            villaMovie = Movie.decodeStream(is);
            is = context.getResources().openRawResource(armyGif);
            runMovie = Movie.decodeStream(is);
            is = context.getResources().openRawResource(deathGif);
            dieMovie = Movie.decodeStream(is);
            is = context.getResources().openRawResource(trainingGif);
            trainingMovie = Movie.decodeStream(is);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
        }

    }
}
