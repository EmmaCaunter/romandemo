package com.example.webdev11.museumdemo;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.List;

/**
 * Created by webdev11 on 2016-07-18.
 */
public class CameraSurface extends SurfaceView implements SurfaceHolder.Callback {

    public static String TAG = "CAMERA_SURFACE";
    private Activity activity;
    private SurfaceHolder holder;
    private Camera camera;
    private  boolean isCameraAvailable;

    public CameraSurface(Activity activity) {
        super(activity);
        this.activity = activity;
        if (checkCameraHardware(activity)) {
            Camera cam = getCameraInstance();
            if (cam != null) {
                this.isCameraAvailable = true;
                this.camera = cam;
                this.holder = getHolder();
                this.holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
                this.holder.addCallback(this);
            } else {
                this.isCameraAvailable = false;
            }
        }

    }

    public Camera.Parameters getCameraParameters() {
        return this.camera.getParameters();
    }

    public boolean isCameraAvailable() {
        return this.isCameraAvailable;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }
        this.camera.setDisplayOrientation((info.orientation - degrees + 360) % 360);

        try {
            this.camera.setPreviewDisplay(this.holder);
        } catch (IOException e) {
            Log.e(TAG, "surfaceCreated exception: ", e);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Camera.Parameters params = this.camera.getParameters();
        List<Camera.Size> prevSizes = params.getSupportedPreviewSizes();
        for (Camera.Size s : prevSizes)
        {
            if((s.height <= height) && (s.width <= width))
            {
                params.setPreviewSize(s.width, s.height);
                break;
            }
        }

        this.camera.setParameters(params);
        this.camera.startPreview();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        this.camera.stopPreview();
        this.camera.release();
    }

    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    /** Check if this device has a camera */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            // this device has a camera
            return true;
        } else {
            // no camera on this device
            return false;
        }
    }
}
